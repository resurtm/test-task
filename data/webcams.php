<?php

return [
    1 => [
        'id' => 1,
        'title' => 'Web Camera #1',
        'frames' => [
            'webcam1-1.jpg',
            'webcam1-2.jpg',
            'webcam1-3.jpg',
        ],
    ],
    2 => [
        'id' => 2,
        'title' => 'Web Camera #2',
        'frames' => [
            'webcam2-1.jpg',
            'webcam2-2.jpg',
            'webcam2-3.jpg',
        ],
    ],
];
