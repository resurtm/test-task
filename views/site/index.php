<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $webcams array */

$this->title = 'Avira Look-Out';
?>
<div class="site-index">

    <h1>Avira Look-Out</h1>

    <div class="row">
        <div class="col-lg-3">
            <form id="webcam-form">
                <div class="form-group">
                    <label for="webcam-selector">Active Web Camera</label>
                    <?= Html::dropDownList(
                        'webcam-selector',
                        '',
                        ArrayHelper::map($webcams, 'id', 'title'),
                        ['prompt' => '(select web camera)', 'class' => 'form-control', 'id' => 'webcam-selector']
                    ) ?>
                </div>
                <button type="submit" class="btn btn-primary btn-lg">Change Web Camera</button>
            </form>
        </div>

        <div class="col-lg-9">
            <img src="#" alt="" id="webcam-image" class="hide"/>
        </div>
    </div>

</div>
