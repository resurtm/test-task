jQuery(function ($) {
    var form = $('#webcam-form');
    var image = $('#webcam-image');
    if (form.length === 0 || image.length === 0) {
        return;
    }

    var interval = null;

    form.on('submit', function (e) {
        e.preventDefault();

        var id = form.find('#webcam-selector').val();
        toggleWebCamera(id.length === 0 ? null : id);

        return false;
    });

    function toggleWebCamera(id) {
        if (id === null) {
            image.attr('src', '#').addClass('hide');
            stopWebCameraRefresh();
        } else {
            image
                .attr('src', '/webcam' + id)
                .attr('data-src', '/webcam' + id)
                .removeClass('hide');
            startWebCameraRefresh();
        }
    }

    function startWebCameraRefresh() {
        if (interval !== null) {
            return;
        }
        interval = setInterval(function () {
            image.attr(
                'src',
                image.attr('data-src') + '?' + new Date().getTime()
            );
        }, 5000);
    }

    function stopWebCameraRefresh() {
        clearInterval(interval);
        interval = null;
    }
});
